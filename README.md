# DBE_IoT_WS22-23


## Getting started

Check out my Wiki!


- [1. Einführung Projekt](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/1.-Einf%C3%BChrung-Projekt)
- [2. Aufgabe 1: Vorgegebene Versuchsaufbauten](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/2.-Aufgabe-1:-Vorgegebene-Versuchsaufbauten)
 - [2.1 Aufgabe 1.1: Thermische Untersuchung eines Wohnraums](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/2.1-Aufgabe-1.1:-Thermische-Untersuchung-eines-Wohnraums)
 - [2.2 Aufgabe 1.2: Nutzungsroutine von Raumbeleuchtungen](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/2.2-Aufgabe-1.2:-Nutzungsroutine-von-Raumbeleuchtungen)
- [3 Aufgabe 2: Eigener Versuchsaufbau](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/3-Aufgabe-2:-Eigener-Versuchsaufbau)
- [4 Aufgabe 3: Fragestellungen zur IoT Vorlesung](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/wikis/4-Aufgabe-3:-Fragestellungen-zur-IoT-Vorlesung)



## Used files

- [Jupyter notebook](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/blob/main/Data_Analytics_Sitter.ipynb)
- [Log files zip archive](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/blob/main/wotlogs_rotate.zip)
- [Outdoor temperature csv](https://gitlab.reutlingen-university.de/sitter/dbe_iot_ws22-23/-/blob/main/outdoor_temp.csv)



